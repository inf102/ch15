﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace part1
{

  class EventManager
  {
    private readonly Dictionary<object, List<Action<object>>> _subscriptions;

    public EventManager()
    {
      _subscriptions = new Dictionary<object, List<Action<object>>>();
    }

    public void subscribe(object event_type, Action<object> handler)
    {
      if(!_subscriptions.ContainsKey(event_type))
        _subscriptions.Add(event_type, new List<Action<object>>(new[] {handler}));
      else _subscriptions[event_type].Add(handler);
    }

    public void publish(object @event)
    {
      var evt = @event is object[] ? (@event as object[])[0] : @event;
      var arg = @event is object[] ? (@event as object[]).Skip(1).ToArray() : null;
      if(_subscriptions.ContainsKey(evt))
        for (var i = 0; i < _subscriptions[evt].Count; ++i)
          _subscriptions[evt][i].Invoke(arg);
    }
  }

  class DataStorage
  {
    private string[] _data;
    EventManager _event_manager;

    public DataStorage(EventManager event_manager)
    {
      _event_manager = event_manager;
      _event_manager.subscribe("load", load);
      _event_manager.subscribe("start", produce_words);
    }

    private void load(object obj)
    {
      var file_to_path = (obj as object[])[0] as string;
      using (var f = File.OpenText(file_to_path))
        _data = Regex.Split(f.ReadToEnd().ToLower(), "[\\W_]+");
    }

    private void produce_words(object obj)
    {

      foreach (var w in _data)
          _event_manager.publish(new [] { "word", w});
        _event_manager.publish(new [] { "eof", null });
    }
  }

  class StopWordFilter
  {
    private EventManager _event_manager;
    private string[] _stop_words;

    public StopWordFilter(EventManager event_manager)
    {
      _event_manager = event_manager;
      _event_manager.subscribe("load", load);
      _event_manager.subscribe("word", is_stop_word);
    }

    public void load(object arg)
    {
      using (var f = File.OpenText("../stop_words.txt"))
      {
        var t = f.ReadToEnd().ToLower().Split(',').ToList();
        t.AddRange("a b c d e f g h i j k l m n o p q r s t u v w x y z".Split(' '));
        _stop_words = t.ToArray();
      }
    }

    public void is_stop_word(object arg)
    {
      var word = (arg as object[])[0];
      if(!_stop_words.Contains(word))
        _event_manager.publish(new []{ "valid_word", word });
    }
  }

  class WordFrequencyCounter
  {
    private EventManager _event_manager;
    Dictionary<string, int> _word_freq = new Dictionary<string, int>();

    public WordFrequencyCounter(EventManager event_manager)
    {
      _event_manager = event_manager;
      _event_manager.subscribe("valid_word", increment_count);
      _event_manager.subscribe("print", print_freqs);
    }

    public void increment_count(object obj)
    {
      var word = (obj as object[])[0] as string;
      if (_word_freq.ContainsKey(word)) _word_freq[word]++;
      else _word_freq.Add(word, 1);
    }

    public void print_freqs(object arg)
    {
      foreach (var w in _word_freq.OrderByDescending(p => p.Value).Take(25))
        Console.WriteLine($"{w.Key}  -  {w.Value}");
    }

  }

  class WordFrequencyApplication
  {
    private EventManager _event_manager;

    public WordFrequencyApplication(EventManager _event_manager)
    {
      this._event_manager = _event_manager;
      _event_manager.subscribe("run", run);
      _event_manager.subscribe("eof", stop);

    }

    public void run(object obj)
    {
      var path_to_file = (obj as object[])[0];
      _event_manager.publish(new [] { "load", path_to_file });
      _event_manager.publish(new [] { "start", null});
    }

    public void stop(object obj)
    {
      _event_manager.publish(new [] {"print", null});
    }
  }

    class Program
    {
      static void Main(string[] args)
      {
        var em = new EventManager();
        new DataStorage(em); new StopWordFilter(em); new WordFrequencyCounter(em);
        new WordFrequencyApplication(em);
        em.publish(new []{"run", args[0] });
      }
    }

}


